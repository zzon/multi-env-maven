package com.songce.common.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 多环境变量工具类
 *
 * @author Scott Chen
 * @date 2023-12-20
 * @since 1.0.0
 */
public class ProfileUtils {

    public static Map<String, String> profileData() {
        String profile = SpringContextUtils.getActiveProfile();

        String[] temp = profile.split("-");

        Map<String, String> map = new HashMap<>();
        map.put("platform", temp[0]);
        map.put("env", temp[1]);
        return map;
    }

    /**
     * 当前环境，不区分平台
     *      大写返回
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String envUpperCase() {
        Map<String, String> map = profileData();
        String env = map.get("env");
        return env.toUpperCase();
    }

    /**
     * 当前环境，不区分平台
     *      小写返回
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String envLowerCase() {
        Map<String, String> map = profileData();
        String env = map.get("env");
        return env.toLowerCase();
    }

    /**
     * 小写并中横线
     *      如：zq-test
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String profileDataHyphen() {
        Map<String, String> map = profileData();
        return map.get("platform").toLowerCase() + "-" + map.get("env").toLowerCase();
    }

    /**
     * 小写并下横线
     *      如：zq_test
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String profileDataUnderscore() {
        Map<String, String> map = profileData();
        return map.get("platform").toLowerCase() + "_" + map.get("env").toLowerCase();
    }

    /**
     * 转为大写并集合
     *      如：ZQ，TEST
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static Map<String, String> profileDataUpperCase() {
        Map<String, String> map = profileData();

        map.put("platform", map.get("platform").toUpperCase());
        map.put("env", map.get("env").toUpperCase());
        return map;
    }

    /**
     * 转为大写并中横线
     *      如：ZQ-TEST
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String profileDataUpperCaseHyphen() {
        Map<String, String> map = profileData();
        return map.get("platform").toUpperCase() + "-" + map.get("env").toUpperCase();
    }

    /**
     * 转为大写并下横线
     *      如：ZQ_TEST
     *
     * @author Scott Chen
     * @since 1.0.0
     * @date 2023-12-20
     * @return
     */
    public static String profileDataUpperCaseUnderscore() {
        Map<String, String> map = profileData();
        return map.get("platform").toUpperCase() + "_" + map.get("env").toUpperCase();
    }

}
