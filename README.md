# Spring MVC使用Maven多环境部署

## Spring XML代码化
1. Spring MVC配置全部进行代码化，去除全部Spring XML配置，同时在web.xml中去除Spring入口相关配置
2. 支持全平台通用配置，支持不同平台各自通用配置，两者不一样

## 多平台多环境
1. 支持`多平台`（xx），`平台`用例名称：p1，p2
2. 支持`多环境`，`多环境`用例名称：dev，test，prod
3. `平台多环境`，`平台多环境`用例名称：p1-dev，p1-test，p1-prod，p2-dev，p2-test，p2-prod

# 打包和运行
## mvn打包运行
1. 构建
    + 构建命令：mvn clean package -P [xx-dev | xx-test | xx-prod]
    + `-P`参数配置指定要激活的某个`profile`（配置在`pom.xml`中的`<profile id>`）
    + `xx`为多平台名称
    + 用例，对`p2平台`的`生产环境`构建命令：`mvn clean package -P p2-prod`
2. Tomcat运行
    + 如上打好的包，可以直接在IDEA中使用Tomcat部署运行，也可部署在外部Tomcat中直接运行。
    + 注意：`IDEA`中`Tomcat`配置界面下面的`Before launch`下如果有`Build`相关构建命令，请全部删除，因为前面使用了命令行构建，此处`IDEA`的`Build`命令是未加我们自己所使用的`平台环境参数`的。
    + 如果不使用`mvn clean package -P`先构建，而直接使用Tomcat运行，不会成功，即使配置`VM options`运行参数：`-DprofileActive=[xx-dev | xx-test | xx-prod] -Dplatform=xx -Denv=[dev | test | prod]`，也因没有替换`web.xml`中的变量，而无法直接运行；
3. `IDEA`直接运行
    + `IDEA`编辑器要想直接正常运行已打包好的`jar`或`war`，不能在`run | debug`前不能再次构建项目，因为IDEA使用的默认构建命令不是我们打包所使用的`mvn`相关指令，这样会造成已经替换正确的配置文件，又会被原始配置文件全部覆盖还原回去；
    + 去除`run | debug`前构建方法如下
        - 打开`IDEA`的`Run/Debug Configurations`配置界面；
        - 对应项目下，点击`Modify options`，弹出多选择配置弹窗`Add Run Options`；
        - 找到并勾选`Do not build before run`；
        - 点`OK`关闭配置界面，通过`IDEA`运行对应项目，就可以直接`run | debug`了；

## 使用maven插件`tomcat7-maven-plugin`构建及部署运行
1. 使用`IDEA`中的`Tomcat`启动，`maven`是不会被执行，所以`web.xml`文件内的变量也不会被替换；
2. 使用maven插件`tomcat7-maven-plugin`的Tomcat进行部署，可以使用`maven`构建部署运行；
3. 构建和启动命令：`mvn clean package -P [xx-dev|xx-test|xx-prod] tomcat7:run-war -DprofileActive=[xx-dev | xx-test | xx-prod]`；


# 多平台多环境实现
## 业务配置文件创建
1. 在项目`resources/conf`目录下创建平台目录和环境目录，`/xx/dev`，`/xx/test`，`/xx/prod`多平台多环境目录；
2. 在不同环境目录下创建对应的多环境配置文件，为了区分和规范，请在每个文件名后加上`-dev`|`-test`|`-prod`；
3. 配置文件路径用例：某平台测试环境某个配置文件，`resources/conf/xx/test/foo-test.properties`

## Spring配置
1. 在`spring`的`xml`配置文件（任意）中`beans`节点内，创建多环境`profile`属性，值为`default`|`xx-dev`|`xx-test`|`xx-prod`；
2. 在每个`beans`节点下，按常规`spring`要求配置；
3. 如果有通用的配置，每一个`profile`节点都要求放置；

## web.xml文件配置
1. `war`包内资源占位符`${xxx}`的替换，由`maven-war-plugin`插件`<webResources>`执行，当然也包括`web.xml`中占位符`${xxx}`的替换;
    + 【不能省，必须】配置`maven-war-plugin`插件下的`webXml`属性为源码全路径，如`src/main/webapp/WEB-INF/web.xml`；
    + 构建时，通过`mvn package -P`命令和指定`pom.xml`文件内的某个环境`profile id`作为`多平台多环境`参数，则`profile id`下的全部属性通过`maven-war-plugin`插件的`<webResources>`对web资源进行构建替换；
    + 如果不构建而直接运行，请使用`tomcat7-maven-plugin`插件，参照如上命令构建运行；
2. `web.xml`中配置`多平台多环境``spring.profiles.default`，增加`<context-param>`参数`spring.profiles.default`，其值为`某个默认多平台多环境`；
2. `web.xml`中配置`多平台多环境``spring.profiles.active`，增加`<context-param>`参数`spring.profiles.active`，其值为`${profileActive}`；
3. `web.xml`中配置`多平台``platform`，增加`<context-param>`参数`platform`，值为`${platform}`；
4. `web.xml`中配置`多环境``env`，增加`<context-param>`参数`env`，值为`${env}`；
5. 其中变量值`${profileActive}`、`${platform}`、`${env}`，均由`maven`的`pom.xml`文件中`profile`节点下定义的属性`profileActive`、`platform`、`env`对应的值进行替换；
6. `spring.profiles.active`优先级大于`spring.profiles.default`；

## Maven构建文件pom.xml
1. `多平台多环境`的配置说明；
    + 如果有多个模块涉及到`多平台多环境`切换，就必须在`每个对应模块`的`pom.xml`进行`多平台多环境`的全部配置；
    + 请区分是否为`web`模块和`非web`模块，`web`模块涉及到`/WEB-INFO/目录`下`web.xml及其它资源文件`的处理；
    + `非web模块`（即只有服务端）不用处理`web.xml及其它资源文件`，只需要使用`<include>`，不用拼接`<targetPath>`，按需要配置`原路径复制`文件，并区分好多平台多环境；
2. 对于涉及`多平台多环境`的每个模块而言，都必须有一套`多平台多环境`属性定义；
    + `多平台多环境`属性定义在`pom.xml`配置文件的`<profiles>`-->`<profile>`-->`<properties>`下；
    + 每个平台下的每个环境都对应着一个`<profile>`，其定义的全部值，与代码中的`profile`定义的全部值对应；
    + 多平台多环境属性定义格式为
        + ```xml
            <properties>
                <platform>p1</platform>
                <env>dev</env>
                <!-- 替换web.xml中的profileActive, 作为spring的环境变量, 以启动不同的环境 -->
                <profileActive>p1-dev</profileActive>
            </properties>
            <activation>
                <!-- 外部命令行传入激活环境，此处配置false -->
                <activeByDefault>false</activeByDefault>
             </activation>
        ```
    + 定义`多平台`属性，其值为某个`平台`名，即`<platform>p1</platform>`，`platform`为平台`key`，`p1`为平台`value`；
    + 定义`多环境`属性，其值为某个`环境`名，即`<env>test</env>`，`env`为环境`key`，`test`为环境`value`；
    + 定义`平台环境激活`属性，其值为某个`平台环境`名，即`<profileActive>p1-test</profileActive>`，`profileActive`为平台环境激活`key`，`p1-test`为平台环境激活`value`；
    + 属性定义`<profileActive>p1-test</profileActive>`，其值`p1-test`也用来替换`web.xml`中对应的`${profileActive}`变量；
    + `外部命令行传入激活环境`，不启用默认激活环境，`<profiles>`-->`<profile>`-->`<activeByDefault>`全部配置为`false`，不配置默认启动；
    + `<profiles>`-->`<profile>`-->`<properties>`配置说明
        - `<properties>`下你可以定义任何想要的其它属性`<key>value</key>`。在项目构建时，其定义的属性`key`如果在`pom.xml`，甚至是其它项目文件中，发现有对应的`key`，能自动替换为`key`对应的`value`；
3. 针对`jar`包的资源构建
    + 使用`maven-resources-plugin`插件配置`jar`包中的占位符替换；
    + 在`<build>`-->`<resources>`节点进行配置
    + 路径拼接
        - `<resource>`节点下的`<exclude>`、`<include>`（路径后段）节点的路径与`<directory>`或`<targetPath>`（路径前段）节点的路径拼接后，必须能找得到对应文件；
    + 资源文件构建，注意，只要涉及到多平台多环境，不论有多少个模块，都必须按如下要求配置。
        1. `构建时先排除文件`，构建时，多平台多环境部署为了只加载当前生效环境的配置文件，所以先排除全部多平台多环境的配置文件；
        2. `构建通用配置文件`，构建时，先加载每个平台通用配置文件；
        3. `构建当前激活环境的配置文件`，再加载当前激活环境的配置文件；
        4. `构建其它资源文件`，其它资源文件会自动复制到对应目录，可以不再配置，如`*.xml`，`*.ttf`，`*.jar`，`*.xles`等等；
4. 针对`war`包的资源构建
    + 使用`maven-war-plugin`插件配置`war`包中的占位符替换；
    + `maven-war-plugin`下`<configuration>`-->`<webResources>`节点进行配置；
    + 配置如下，包括对`web.xml`文件内的`${profileActive}`进行变量值替换，替换为当前生效的环境。
        ```xml
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
            <configuration>
                <warSourceDirectory>src/main/webapp</warSourceDirectory>
                <webXml>src/main/webapp/WEB-INF/web.xml</webXml>
                <failOnMissingWebXml>false</failOnMissingWebXml>

                <!-- war包资源构建 -->
                <webResources>
                    <resource>
                        <directory>src/main/webapp/WEB-INF</directory>
                        <!-- 构建中允许替换 -->
                        <filtering>true</filtering>
                        <includes>
                            <!-- 构建的源文件 -->
                            <!-- 【注意】针对web.xml的资源构建，其<include>的值只能是文件名web.xml，不能带路径，否则web.xml文件内的变量替换会失败 -->
                            <!-- 【注意】<include>的路径与<directory>进行拼接，也要与<targetPath>路径进行拼接 -->
                            <include>web.xml</include>
                        </includes>
                        <!-- 构建到目标目录 -->
                        <!-- include的路径与targetPath路径进行拼接 -->
                        <targetPath>WEB-INF</targetPath>
                    </resource>
                </webResources>
            </configuration>
        </plugin>
        ```

# 运行
1. 到目前为止，可以使用mvn构建运行了；
    + 构建命令：（如上）；
    + 运行：（如上）；
2. 使用maven插件`tomcat7-maven-plugin`部署运行（如上）



