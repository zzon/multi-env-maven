package com.songce.config.db.entity;

import java.io.Serializable;

/**
 * @author Scott Chen
 * @date 2023-12-26
 * @since 1.0.0
 */
public class DbProperties implements Serializable {
    private static final long serialVersionUID = 3893470610767163085L;

    private String url;
    private String username;
    private String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
