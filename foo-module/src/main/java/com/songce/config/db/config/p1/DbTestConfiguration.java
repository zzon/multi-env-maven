package com.songce.config.db.config.p1;

import com.songce.config.db.entity.DbProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-25
 * @since 1.0.0
 */
@Profile({"p1-test"})
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/${env}/db-${env}.properties"
}, encoding="UTF-8")
public class DbTestConfiguration {

    @Value("${p1.test.db.url}")
    private String url;
    @Value("${p1.test.db.username}")
    private String username;
    @Value("${p1.test.db.password}")
    private String password;

    @Bean
    public DbProperties dbProperties() {
        DbProperties properties = new DbProperties();
        properties.setUrl(url);
        properties.setUsername(username);
        properties.setPassword(password);
        return properties;
    }

}
