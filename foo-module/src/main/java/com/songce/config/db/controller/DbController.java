package com.songce.config.db.controller;

import com.songce.config.db.entity.DbProperties;
import com.songce.common.PageUtils;
import com.songce.common.R;
import com.songce.util.jackson.JacksonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 多环境打包测试
 */
@RestController
public class DbController {
    private static final Logger logger = LoggerFactory.getLogger(DbController.class);

    private DbProperties dbProperties;

    @Autowired
    public void setDbProperties(DbProperties dbProperties) {
        this.dbProperties = dbProperties;
    }

    @RequestMapping(value = "/env/db")
    public R envTest() {
        logger.debug("---------- 多环境打包测试 ----------");

        Map<String, String> map = new HashMap<>();
        List<Map> list = new ArrayList<>();

        if (dbProperties == null) {
            logger.debug("dbProperties为空");
        } else {
            logger.debug("dbProperties不为空, {}", JacksonUtils.toJsonStr(dbProperties));
        }


        map.put("url", dbProperties.getUrl());
        map.put("username", dbProperties.getUsername());
        map.put("password", dbProperties.getPassword());

        list.add(map);

        PageUtils pageUtil = new PageUtils(list, map.size(), 20, 1);
        return R.ok().put("page", pageUtil);
    }

}
