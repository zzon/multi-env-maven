package com.songce.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.songce.util.jackson.JacksonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * HTTP 请求响应消息转换
 * @author Scott Chen
 * @since 1.0
 * 2018-02-03
 */
@Configuration
public class HttpMsgConverter {
    private static final Logger logger = LoggerFactory.getLogger(HttpMsgConverter.class);


    @Bean ObjectMapper customObjectMapper() {
        return JacksonUtils.objectMapper();
    }


    /**
     * jackson进行消息转换时,处理中文乱码
     * @return
     */
    @Bean
    public HttpMessageConverter httpMessageConverter() {
        StringHttpMessageConverter messageConverter = new StringHttpMessageConverter();
        messageConverter.setSupportedMediaTypes(mediaTypes());
        messageConverter.setDefaultCharset(Charset.forName("UTF-8"));
        return messageConverter;
    }

    /**
     * 处理中文乱码
     * @return
     */
    public List<MediaType> mediaTypes() {
        //处理中文乱码
        List<MediaType> list = new LinkedList();
        // application/json 一定要放到前面
        list.add(MediaType.APPLICATION_JSON);
        list.add(MediaType.TEXT_HTML);
        list.add(MediaType.APPLICATION_XML);
        return list;
    }


    /**
     * HTTP 请求消息转换器
     * 处理Date日期类型,处理LocalDateTime日期类型
     * 使用<code>MappingJackson2HttpMessageConverter</code>类
     * @return
     */
    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setObjectMapper(customObjectMapper());
        messageConverter.setSupportedMediaTypes(mediaTypes());

        logger.debug("--- 创建MappingJackson2HttpMessageConverter Bean");
        return messageConverter;
    }


}
