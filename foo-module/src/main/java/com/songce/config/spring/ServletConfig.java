package com.songce.config.spring;

import com.songce.config.db.entity.DbProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.*;

/**
 * Spring MVC Servlet 配置
 *
 * @author Scott Chen
 * @date 2023-12-27
 * @since 1.0.0
 */
@Configuration
// 实现 <mvc:annotation-driven />
@EnableWebMvc
// 实现 <context:component-scan base-package="com.songce" />
@ComponentScan(basePackages = {"com.songce"})
// xml文件已代码化
// 如果确实有业务xml不能代码化，可以使用 @ImportResource 引入
/*@ImportResource(value = {
        "classpath:spring/spring-main.xml"
})*/
public class ServletConfig implements WebMvcConfigurer {


    /**
     * 实现 <mvc:default-servlet-handler/>
     *
     * @param configurer
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
        WebMvcConfigurer.super.configureDefaultServletHandling(configurer);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 对全部请求路径进行跨域处理
        registry.addMapping("/**")
                // 允许客户端发送认证信息
                .allowCredentials(true)
                // 支持的域
                .allowedOriginPatterns("*")
                // 允许的请求头，默认允许所有的请求头
                .allowedHeaders("*")
                // 允许的方法，默认允许GET, HEAD, and POST
                .allowedMethods("*")
                // 缓存请求有效时间，单位秒
                .maxAge(3600);
        WebMvcConfigurer.super.addCorsMappings(registry);
    }

    /**
     * 静态资源访问
     * @param registry
     */
    /*@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // 静态文件可以不放在 /src/main/resources/ 下面的 /static/ 目录内，但打包时，必须打进 /src/main/resources/ 目录
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");

        // 静态文件放在 /src/main/webapp/ 下面的 /static/ 目录内，且打包时，不打进 /src/main/resources/ 目录
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");

    }*/

    /*@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
                .indentOutput(true)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd"))
                .modulesToInstall(new ParameterNamesModule());
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
        converters.add(new MappingJackson2XmlHttpMessageConverter(builder.createXmlMapper(true).build()));
    }*/

    /*@Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.mediaType("json", MediaType.APPLICATION_JSON);
        configurer.mediaType("xml", MediaType.APPLICATION_XML);
    }*/

    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LocaleChangeInterceptor());
        registry.addInterceptor(new ThemeChangeInterceptor()).addPathPatterns("/**").excludePathPatterns("/admin/**");
    }*/


}
