package com.songce.config.spring;

import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Spring MVC 根节点配置
 *
 * @author Scott Chen
 * @date 2023-12-27
 * @since 1.0.0
 */
@Configuration
// xml文件已代码化
/*@ImportResource(value = {
        "classpath:spring/spring-context.xml"
})*/
public class RootConfig {

}
