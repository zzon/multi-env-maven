package com.songce.config.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-28
 * @since 1.0.0
 */
@Configuration
@PropertySource(value = {
        "classpath:conf/common/common.properties"
}, encoding="UTF-8")
public class CommonConfig {

    @Value("${common.good}")
    private String good;

    @Bean
    public CommonProperties commonProperties() {
        CommonProperties properties = new CommonProperties();
        properties.setGood(good);
        return properties;
    }

}
