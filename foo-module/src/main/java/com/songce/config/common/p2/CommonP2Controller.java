package com.songce.config.common.p2;

import com.songce.common.PageUtils;
import com.songce.common.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 多环境拆分 Demo
 *
 * @author Scott Chen
 * @since 1.0.0
 * @date 2023-12-28
 */
@Profile({"p2-dev","p2-test","p2-prod"})
@RestController
public class CommonP2Controller {
    private static final Logger logger = LoggerFactory.getLogger(CommonP2Controller.class);

    private CommonP2Properties commonP2Properties;

    @Autowired
    public void setCommonP2Properties(CommonP2Properties commonP2Properties) {
        this.commonP2Properties = commonP2Properties;
    }

    @RequestMapping(value = "/env/good/p2")
    public R envGoodP2() {
        logger.debug("---------- 多环境通用属性测试 ----------");

        Map<String, String> map = new HashMap<>();
        List<Map> list = new ArrayList<>();

        map.put("p2 common hot", commonP2Properties.getHot());

        list.add(map);

        PageUtils pageUtil = new PageUtils(list, map.size(), 20, 1);
        return R.ok().put("page", pageUtil);
    }

}
