package com.songce.config.common;

import java.io.Serializable;

/**
 * @author Scott Chen
 * @date 2023-12-28
 * @since 1.0.0
 */
public class CommonProperties implements Serializable {
    private static final long serialVersionUID = 8346284479563853515L;

    private String good;

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

}
