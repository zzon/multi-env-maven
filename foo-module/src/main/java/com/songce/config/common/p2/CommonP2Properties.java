package com.songce.config.common.p2;

import org.springframework.context.annotation.Profile;

import java.io.Serializable;

/**
 * @author Scott Chen
 * @date 2023-12-28
 * @since 1.0.0
 */
@Profile({"p2-dev","p2-test","p2-prod"})
public class CommonP2Properties implements Serializable {

    private static final long serialVersionUID = -222418369856219809L;

    private String hot;

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

}
