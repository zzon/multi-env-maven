package com.songce.config.common.p1;

import com.songce.common.PageUtils;
import com.songce.common.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 多环境拆分 Demo
 *
 * @author Scott Chen
 * @since 1.0.0
 * @date 2023-12-28
 */
@Profile({"p1-dev","p1-test","p1-prod"})
@RestController
public class CommonP1Controller {
    private static final Logger logger = LoggerFactory.getLogger(CommonP1Controller.class);

    private CommonP1Properties commonP1Properties;

    @Autowired
    public void setCommonP1Properties(CommonP1Properties commonP1Properties) {
        this.commonP1Properties = commonP1Properties;
    }

    @RequestMapping(value = "/env/good/p1")
    public R envGoodP1() {
        logger.debug("---------- 多环境通用属性测试 ----------");

        Map<String, String> map = new HashMap<>();
        List<Map> list = new ArrayList<>();

        map.put("p1 common cool", commonP1Properties.getCool());

        list.add(map);

        PageUtils pageUtil = new PageUtils(list, map.size(), 20, 1);
        return R.ok().put("page", pageUtil);
    }

}
