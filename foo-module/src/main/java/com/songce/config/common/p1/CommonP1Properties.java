package com.songce.config.common.p1;

import org.springframework.context.annotation.Profile;

import java.io.Serializable;

/**
 * @author Scott Chen
 * @date 2023-12-28
 * @since 1.0.0
 */
@Profile({"p1-dev","p1-test","p1-prod"})
public class CommonP1Properties implements Serializable {

    private static final long serialVersionUID = 3740382313924008749L;

    private String cool;

    public String getCool() {
        return cool;
    }

    public void setCool(String cool) {
        this.cool = cool;
    }

}
