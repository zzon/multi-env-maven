package com.songce.config.common.p1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-28
 * @since 1.0.0
 */
@Profile({"p1-dev","p1-test","p1-prod"})
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/common/common-${platform}.properties"
}, encoding="UTF-8")
public class CommonP1Config {

    @Value("${common.cool}")
    private String cool;

    @Bean
    public CommonP1Properties commonP1Properties() {
        CommonP1Properties properties = new CommonP1Properties();
        properties.setCool(cool);
        return properties;
    }

}
