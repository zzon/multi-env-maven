package com.songce.biz.foo.config.p1;

import com.songce.biz.foo.entity.FooProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-25
 * @since 1.0.0
 */
@Profile("p1-test")
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/${env}/foo-${env}.properties"
}, encoding="UTF-8")
public class FooTestConfiguration {

    @Value("${p1.test.foo.key}")
    private String key;
    @Value("${p1.test.foo.value}")
    private String value;

    @Bean
    public FooProperties fooProperties() {
        FooProperties properties = new FooProperties();
        properties.setKey(key);
        properties.setValue(value);
        return properties;
    }

}
