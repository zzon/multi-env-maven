package com.songce.biz.foo.entity;

import java.io.Serializable;

/**
 * 多环境拆分 Demo
 *
 * @author Scott Chen
 * @since 1.0.0
 * @date 2022-10-26
 */
public class FooProperties implements Serializable {
    private static final long serialVersionUID = -8290002480870979330L;

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
