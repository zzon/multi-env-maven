package com.songce.biz.foo.controller;

import com.songce.biz.foo.service.FooService;
import com.songce.common.PageUtils;
import com.songce.common.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 多环境拆分 Demo
 *
 * @author Scott Chen
 * @since 1.0.0
 * @date 2022-10-26
 */
@RestController
public class FooController {
    private static final Logger logger = LoggerFactory.getLogger(FooController.class);


    private FooService fooService;

    @Autowired
    public void setFooService(FooService fooService) {
        this.fooService = fooService;
    }


    @RequestMapping(value = "/env/foo")
    public R envTest() {
        logger.debug("---------- 多环境打包测试 ----------");

        Map<String, String> map = new HashMap<>();
        List<Map> list = new ArrayList<>();

        map.put("foo key", fooService.getFooKey());
        map.put("foo value", fooService.getFooValue());

        list.add(map);

        PageUtils pageUtil = new PageUtils(list, map.size(), 20, 1);
        return R.ok().put("page", pageUtil);
    }

}
