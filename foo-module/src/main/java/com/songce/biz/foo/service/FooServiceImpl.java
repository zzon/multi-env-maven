package com.songce.biz.foo.service;

import com.songce.biz.foo.entity.FooProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 多环境拆分 Demo
 *
 * @author Scott Chen
 * @since 1.0.0
 * @date 2022-10-26
 */
@Component
public class FooServiceImpl implements FooService {

    private static final Logger logger = LoggerFactory.getLogger(FooServiceImpl.class);

    private FooProperties fooProperties;

    @Autowired
    public void setFooProperties(FooProperties fooProperties) {
        this.fooProperties = fooProperties;
    }


    @Override
    public String getFooKey() {
        logger.debug("foo key: {}", fooProperties.getKey());
        return fooProperties.getKey();
    }

    @Override
    public String getFooValue() {
        logger.debug("foo value: {}", fooProperties.getValue());
        return fooProperties.getValue();
    }


}
