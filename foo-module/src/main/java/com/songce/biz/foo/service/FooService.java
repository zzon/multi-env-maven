package com.songce.biz.foo.service;

public interface FooService {
    String getFooKey();

    String getFooValue();

}
