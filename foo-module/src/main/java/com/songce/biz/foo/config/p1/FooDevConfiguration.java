package com.songce.biz.foo.config.p1;

import com.songce.biz.foo.entity.FooProperties;
import com.songce.common.util.ProfileUtils;
import com.songce.common.util.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

/**
 * @author Scott Chen
 * @date 2023-12-25
 * @since 1.0.0
 */
@Profile({"p1-dev"})
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/${env}/foo-${env}.properties"
}, encoding="UTF-8")
public class FooDevConfiguration {


    @Value("${p1.dev.foo.key}")
    private String key;
    @Value("${p1.dev.foo.value}")
    private String value;

    @Bean
    public FooProperties fooProperties() {
        FooProperties properties = new FooProperties();
        properties.setKey(key);
        properties.setValue(value);
        return properties;
    }

}
