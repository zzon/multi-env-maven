package com.songce.util.date.cvt.ldt.cvt;

import com.songce.util.date.DateConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


/**
 * HTTP请求参数<code>LocalDateTime</code>日期格式转换
 * @author Scott Chen
 * @since 1.0
 * 2018-01-29
 */
public class LocalDateTimeToStringConverter implements Converter<LocalDateTime, String> {
    private static final Logger logger = LoggerFactory.getLogger(LocalDateTimeToStringConverter.class);

    @Override
    public String convert(LocalDateTime source) {
        if(source == null){
            return null;
        }
        String format = DateConstant.DATE_TIME_SECOND;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        return source.format(formatter);
    }

}
