package com.songce.util.date.cvt.ldt.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.songce.util.date.DateConstant;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * LocalDateTime 序列化
 * jackson已经通过jsr310实现了 LocalDateTimeSerializer
 * @see {@link com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer}
 *
 * @author Scott Chen
 * @since 1.0
 * 2018-02-08
 */
public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

    public static final LocalDateTimeSerializer instance = new LocalDateTimeSerializer();

    @Override
    public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String format = DateConstant.DATE_TIME_SECOND;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        gen.writeString(value.format(formatter));
    }

}
