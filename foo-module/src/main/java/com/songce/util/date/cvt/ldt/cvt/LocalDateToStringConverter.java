package com.songce.util.date.cvt.ldt.cvt;

import com.songce.util.date.DateConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


/**
 * HTTP请求参数<code>LocalDate</code>日期格式转换
 * @author Scott Chen
 * @since 1.0
 * 2018-01-29
 */
public class LocalDateToStringConverter implements Converter<LocalDate, String> {
    private static final Logger logger = LoggerFactory.getLogger(LocalDateToStringConverter.class);
    
    @Override
    public String convert(LocalDate source) {
        if(source == null){
            return null;
        }
        String format = DateConstant.DATE_MONTH_DAY;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        return source.format(formatter);
    }

}
