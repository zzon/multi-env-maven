package com.songce.util.date.cvt.tmst;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;

import java.util.Date;

/**
 * @author Scott Chen
 * @since 1.0
 * 2018-02-07
 */
public class DateTimestampWebBindingInitializer implements WebBindingInitializer {

    /**
     * @see WebBindingInitializer#initBinder(WebDataBinder)
     */
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(Date.class, new DateTimestampEditor());
    }
}
