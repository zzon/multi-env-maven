package com.songce.util.date;

/**
 * @author Scott Chen
 * @since 1.0
 * 2018-02-03
 */
public class DateConstant {

    public static final String DATE_TIME_SECOND_ZONE = "yyyy-MM-dd HH:mm:ssZ";
    public static final String DATE_TIME_SECOND_MILL = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_TIME_SECOND = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_MINUTE = "yyyy-MM-dd HH:mm";
    // 格林威治GMT时间
    public static final String DATE_TIME_GMT_EN = "EEE d MMM yyyy HH:mm:ss 'GMT'";
    // CST时间
    public static final String DATE_TIME_CST_US = "EEE MMM d HH:mm:ss 'CST' yyyy";

    public static final String TIME_SECOND_MILL = "HH:mm:ss.SSS";
    public static final String TIME_SECOND = "HH:mm:ss";
    public static final String TIME_MINUTE = "HH:mm";

    public static final String DATE_MONTH_DAY = "yyyy-MM-dd";
    public static final String DATE_MONTH = "yyyy-MM";
    public static final String MONTH_DAY = "MM-dd";

}
