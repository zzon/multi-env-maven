package com.songce.util.date.cvt.ldt.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.songce.util.date.DateConstant;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * LocalTime 序列化
 * jackson已经通过jsr310实现了 LocalTimeSerializer
 * @see {@link com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer}
 *
 * @author Scott Chen
 * @since 1.0
 * 2018-02-08
 */
public class LocalTimeSerializer extends JsonSerializer<LocalTime> {

    public static final LocalTimeSerializer instance = new LocalTimeSerializer();

    @Override
    public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String format = DateConstant.TIME_SECOND;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        gen.writeString(value.format(formatter));
    }

}
