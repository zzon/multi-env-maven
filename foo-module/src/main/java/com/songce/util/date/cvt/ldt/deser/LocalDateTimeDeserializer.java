package com.songce.util.date.cvt.ldt.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.songce.util.date.DateConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * LocalDateTime 反序列化
 * jackson已经通过jsr310实现了 LocalDateTimeDeserializer
 * @see {@link com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer}
 *
 * @author Scott Chen
 * @since 1.0
 * 2018-02-08
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    private static final Logger logger = LoggerFactory.getLogger(LocalDateTimeDeserializer.class);

    public static final LocalDateTimeDeserializer instance = new LocalDateTimeDeserializer();

    private static final List<String> formarts = new LinkedList();
    static {
        formarts.add(DateConstant.DATE_TIME_MINUTE);
        formarts.add(DateConstant.DATE_TIME_SECOND);
        formarts.add(DateConstant.DATE_TIME_SECOND_MILL);
    }

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String source = parser.getText().trim();
        if (source.length() == 0) {
            return null;
        }

        String format = "";

        try {
            if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
                format = formarts.get(0);
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
                format = formarts.get(1);
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{3}$")) {
                format = formarts.get(2);
            } else if (source.matches("^\\d{10}$|^\\d{13}$")) {
                // Windows时间戳(13位),Unix时间戳(10位)
                format = formarts.get(1);
            } else {
                throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
            return LocalDateTime.parse(source, formatter);
        }catch (Exception e) {
            logger.error("参数" + source + ", 通过【"+ this.getClass().getName() +"】转换为日期格式【" + format + "】时, 发生错误");
            e.printStackTrace();
            return null;
        }
    }

}
