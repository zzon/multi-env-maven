package com.songce.util.date.cvt.ldt.cvt;

import com.songce.util.date.DateConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;


/**
 * HTTP请求参数<code>LocalDateTime</code>日期格式转换
 * @author Scott Chen
 * @since 1.0
 * 2018-01-29
 */
public class StringToLocalDateTimeConverter implements Converter<String, LocalDateTime> {
    private static final Logger logger = LoggerFactory.getLogger(StringToLocalDateTimeConverter.class);

    private static final List<String> formarts = new LinkedList();
    static {
        formarts.add(DateConstant.DATE_TIME_MINUTE);
        formarts.add(DateConstant.DATE_TIME_SECOND);
        formarts.add(DateConstant.DATE_TIME_SECOND_MILL);
    }

    @Override
    public LocalDateTime convert(String source) {
        if(source == null){
            return null;
        }

        String format = "";

        try {
            if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
                format = formarts.get(0);
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
                format = formarts.get(1);
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{3}$")) {
                format = formarts.get(2);
            } else if (source.matches("^\\d{10}$|^\\d{13}$")) {
                // Windows时间戳(13位),Unix时间戳(10位)
                format = formarts.get(1);
            } else {
                throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(source, formatter);
        } catch (Exception e){
            logger.error("参数" + source + ", 通过【"+ this.getClass().getName() +"】转换为日期格式【" + format + "】时, 发生错误");
            e.printStackTrace();
            return null;
        }
    }

}
