package com.songce.util.date.cvt.ldt.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.songce.util.date.DateConstant;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * LocalDate 序列化
 * jackson已经通过jsr310实现了 LocalDateSerializer
 * @see {@link com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer}
 *
 * @author Scott Chen
 * @since 1.0
 * 2018-02-08
 */
public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    public static final LocalDateSerializer instance = new LocalDateSerializer();

    @Override
    public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String format = DateConstant.DATE_MONTH_DAY;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        gen.writeString(value.format(formatter));
    }

}
