package com.songce.util.date.cvt.tmst;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Date与Timestamp相互转换
 * 前端是时间戳格式,后端是Date类型
 * @author Scott Chen
 * @since 1.0
 * 2018-02-07
 */
public class DateTimestampEditor extends PropertyEditorSupport {
    /**
     * @see PropertyEditorSupport#setAsText(String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(new Date(Long.decode(text)));
    }

    /**
     * @see PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText() {
        Date value = (Date) getValue();
        return (value != null ? String.valueOf(TimeUnit.MILLISECONDS.toSeconds(value.getTime())) : "");
    }

}
