package com.songce.util.date;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;

/**
 * @author Scott Chen
 * @version 1.0
 * 2017-10-28 11:19
 */
public class LocalDateTimeUtils {

    private LocalDateTimeUtils() {}

    //获取当前时间的LocalDateTime对象
    //LocalDateTime.now();

    //根据年月日构建LocalDateTime
    //LocalDateTime.of();

    //比较日期先后
    //LocalDateTime.now().isBefore(),
    //LocalDateTime.now().isAfter(),

    /**
     * String转LocalDateTime
     * @param src
     * @param formatter
     * @return
     */
    public static LocalDateTime stringToLocalDateTime(String src, DateTimeFormatter formatter) {
       return LocalDateTime.parse(src, formatter);
    }

    /**
     * LocalDateTime转String
     * @param dateTime
     * @param pattern
     * @return
     */
    public static String localDateTimeToString(LocalDateTime dateTime, String pattern) {
        return dateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 将当前LocalDateTime时间按为指定格式转为String
     * @param pattern
     * @return
     */
    public static String formatNow(String pattern) {
        return  localDateTimeToString(LocalDateTime.now(), pattern);
    }

    /**
     * String转LocalDate
     * @param src
     * @param formatter
     * @return
     */
    public static LocalDate stringToLocalDate(String src, DateTimeFormatter formatter) {
        return LocalDate.parse(src, formatter);
    }

    /**
     * LocalDate转String
     * @param time
     * @param pattern
     * @return
     */
    public static String localDateToString(LocalDate time, String pattern) {
        return time.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * String转LocalTime
     * @param src
     * @param formatter
     * @return
     */
    public static LocalTime stringToLocalTime(String src, DateTimeFormatter formatter) {
        return LocalTime.parse(src, formatter);
    }

    /**
     * LocalTime转String
     * @param time
     * @param pattern
     * @return
     */
    public static String localTimeToString(LocalTime time, String pattern) {
        return time.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * Date转换为LocalDateTime
     * @param date
     * @return
     */
    public static LocalDateTime localDateTimeToDate(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    /**
     * LocalDateTime转换为Date
     * @param time
     * @return
     */
    public static Date dateToLocalDateTime(LocalDateTime time) {
        return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
    }


    /**
     * 获取指定日期的毫秒
     * @param time
     * @return
     */
    public static Long getMilliByTime(LocalDateTime time) {
        return time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }


    /**
     * 获取指定日期的秒
     * @param time
     * @return
     */
    public static Long getSecondsByTime(LocalDateTime time) {
        return time.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
    }


    //日期加上一个数,根据field不同加不同值,field为ChronoUnit.*
    public static LocalDateTime plus(LocalDateTime time, long number, TemporalUnit field) {
        return time.plus(number, field);
    }

    //日期减去一个数,根据field不同减不同值,field参数为ChronoUnit.*
    public static LocalDateTime minu(LocalDateTime time, long number, TemporalUnit field) {
        return time.minus(number, field);
    }

    /**
     * 获取两个日期的差  field参数为ChronoUnit.*
     * @param startTime
     * @param endTime
     * @param field  单位(年月日时分秒)
     * @return
     */
    public static long betweenTwoTime(LocalDateTime startTime, LocalDateTime endTime, ChronoUnit field) {
        Period period = Period.between(LocalDate.from(startTime), LocalDate.from(endTime));
        if (field == ChronoUnit.YEARS) {
            return period.getYears();
        }
        if (field == ChronoUnit.MONTHS) {
            return period.getYears() * 12 + period.getMonths();
        }
        return field.between(startTime, endTime);
    }

    //获取一天的开始时间，2017,7,22 00:00
    public static LocalDateTime getDayStart(LocalDateTime time) {
        return time.withHour(0)
                .withMinute(0)
                .withSecond(0)
                .withNano(0);
    }

    //获取一天的结束时间，2017,7,22 23:59:59.999999999
    public static LocalDateTime getDayEnd(LocalDateTime time) {
        return time.withHour(23)
                .withMinute(59)
                .withSecond(59)
                .withNano(999999999);
    }


    /**
     * 纳秒转换
     * 可转为:天/小时/分钟/秒/毫秒
     *
     * @param nanoTime
     * @return
     */
    public static String fromNanoTime(long nanoTime){
        if (nanoTime <= 0) {
            return "0秒";
        }
        double a,b,c,d = 0.000d;
        long day = 0L;  //天
        long HH = 0L;   //小时
        long mm = 0L;   //分
        long ss = 0L;   //秒
        long ms = 0L;   //毫秒

        a = ((double) nanoTime) / 1000000000d / 60d / 60d / 24d;
        if (a > 24) {
            return (long) a + "天";
        }else{
            day = (long) a; //整数部分为天
            b = (a - day) * 24; //小数部分转为小时

            HH = (long) b; //小时
            c = (b - HH) * 60;  //小数部分转为分钟

            mm = (long) c; //分钟
            d = (c - mm) * 60;  //小数部分转为秒

            ss = (long)d; //秒
            ms = (long)((d - ss) * 1000); //毫秒
        }

        String result = "";
        if (day > 0) {
            result = day + "天";
        }
        if (HH > 0) {
            result += HH + "小时";
        }
        if (mm > 0) {
            result += mm + "分钟";
        }
        if (ss > 0) {
            result += ss + "秒";
        }
        if (ms > 0) {
            result += ms + "毫秒";
        }
        return result;
    }


}
