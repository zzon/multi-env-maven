package com.songce.util.date.cvt.ldt.cvt;

import com.songce.util.date.DateConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


/**
 * HTTP请求参数<code>LocalTime</code>日期格式转换
 * @author Scott Chen
 * @since 1.0
 * 2018-01-29
 */
public class LocalTimeToStringConverter implements Converter<LocalTime, String> {
    private static final Logger logger = LoggerFactory.getLogger(LocalTimeToStringConverter.class);

    @Override
    public String convert(LocalTime source) {
        if(source == null){
            return null;
        }
        String format = DateConstant.TIME_SECOND;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.SIMPLIFIED_CHINESE);
        return source.format(formatter);
    }

}
