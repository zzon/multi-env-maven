package com.songce.util.date;


import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Date 工具类
 *
 * 时间区分
 *      CST: 北京时间
 *      GMT: 格林威治时间
 * CST表示四个不同时区（不同语言默认的CST时区不同，可能会造成不一样的时间）
 *      Central Standard Time (USA) UT-6:00
 *      Central Standard Time (Australia) UT+9:30
 *      China Standard Time UT+8:00
 *      Cuba Standard Time UT-4:00
 *      对CST时间而言，java默认的是北京时间，javascript中默认CST是美国中部时间，同一个时间，在前后端转换的时间结果可能不一样；
 * 日期格式
 *      CN: yyyy MM dd
 *      US: MM dd yyyy
 *      EN: dd MM yyyy
 *
 * @author Scott Chen
 * @date 2019-12-12日
 */
public class DateUtils {

    /**
     * 获取当前时间，并指定格式化样式
     * @param formatStyle
     * @return
     */
    public static String nowToString(String formatStyle) {
        return dateToString(Calendar.getInstance().getTime(), formatStyle);
    }

    /**
     * 获得当前系统时间
     *
     * @return
     */
    public static String cstToString() {
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstant.DATE_TIME_SECOND);
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获得当前系统时间
     *
     * @return
     */
    public static String gmtToStringWithCn() {
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstant.DATE_TIME_SECOND);
        // 格林威治所在时区
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获得当前系统时间
     *      EEE MMM d HH:mm:ss 'CST' yyyy
     * @return
     */
    public static String cstToStringWithUs() {
        // 英文显示年月日
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstant.DATE_TIME_CST_US, Locale.US);
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 以GMT方式获取当前系统时间
     *      EEE d MMM yyyy HH:mm:ss 'GMT'
     *
     * @return
     */
    public static String gmtToStringWithEn() {
        // 英文显示年月日
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstant.DATE_TIME_GMT_EN, Locale.US);
        // 所在时区，如下时区可以不写，java默认为GMT+8
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 获取GMT格林威治时间
     *      EEE d MMM yyyy HH:mm:ss 'GMT'
     *
     * @return
     */
    public static String gmtOfUsToStringWithEn() {
        // 英文显示年月日
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstant.DATE_TIME_GMT_EN, Locale.US);
        // 格林威治所在时区
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * 字符串的日期类型 转换为 Date 类型
     *
     * @param timeContent 字符串的日期类型
     * @param formatStyle 日期格式
     * @return
     */
    public static Date stringToDate(String timeContent, String formatStyle) {
        SimpleDateFormat format = new SimpleDateFormat(formatStyle);
        try {
            return format.parse(timeContent);
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return null;
    }

    /**
     * Date 转化成 String 类型的字符串日期格式
     *
     * @param date
     * @param formatStyle 转化成 什么格式
     * @return
     */
    public static String dateToString(Date date, String formatStyle) {
        SimpleDateFormat format = new SimpleDateFormat(formatStyle);
        return format.format(date);
    }

    /**
     * Long 转Date
     * @param timeContent
     * @param formatStyle
     * @return
     */
    public static Date longToDate(Long timeContent, String formatStyle) {
        try {
            return stringToDate(longToDateWithString(timeContent, formatStyle), formatStyle);
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return null;
    }

    /**
     * Long 转String
     * @param timeContent
     * @param formatStyle
     * @return
     */
    public static String longToDateWithString(Long timeContent, String formatStyle) {
        SimpleDateFormat format = new SimpleDateFormat(formatStyle);
        try {
            return format.format(timeContent);
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return null;
    }

    /**
     * 检查日期格式
     *
     * @param time
     * @param format
     * @return
     */
    public static boolean checkDate(String time, String format) {
        if (stringToDate(time, format) == null) {
            return false;
        }
        return true;
    }


    /**
     * 字符串日期格式 转换成 带 地区标识的 Date 类型
     *
     * @param strDate
     * @param locale
     * @param formatStyle
     * @return
     */
    public static Date stringToLocal(String strDate, Locale locale, String formatStyle) {
        SimpleDateFormat srcsdf = new SimpleDateFormat(formatStyle, locale);
        try {
            return srcsdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取本地时间相对的UTC | GMT时间
     *
     * @return
     */
    public static Date getUtcTime() {
        // 1、取得本地时间：
        Calendar calendar = Calendar.getInstance();
        // 2、取得时间偏移量：
        final int zoneOffset = calendar.get(Calendar.ZONE_OFFSET);
        // 3、取得夏令时差：
        final int dstOffset = calendar.get(Calendar.DST_OFFSET);
        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：
        calendar.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return calendar.getTime();
    }

    /**
     * 获取2个时间相差多少秒
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Long getDiffSeconds(Date date1, Date date2) {
        long milliseconds1 = date1.getTime();
        long milliseconds2 = date2.getTime();
        long diff = milliseconds1 - milliseconds2;
        if (diff < 0) {
            diff = -diff;
        }
        long diffSeconds = diff / (1000);
        return diffSeconds;
    }

    /**
     * 获取2个时间相差多少分钟
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Long getDiffMinutes(Date date1, Date date2) {
        Long diffMinutes = getDiffSeconds(date1, date2) / 60;
        return diffMinutes;
    }

    /**
     * 获取2个时间直接 相差多少小时
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Long getDiffHours(Date date1, Date date2) {
        Long diffHours = getDiffMinutes(date1, date2) / 60;
        return diffHours;
    }

    /**
     * 获取2个时间直接 相差多少天
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Long getDiffDays(Date date1, Date date2) {
        Long diffDays = getDiffHours(date1, date2) / 24;
        return diffDays;
    }

    /**
     * 多少天以前
     *
     * @param ago
     * @return
     */
    public static String getDaysAgoStart(int ago) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -ago);
        String time = DateUtils.dateToString(calendar.getTime(), DateConstant.DATE_MONTH_DAY);
        String timeFrom = time + " 00:00:00";
        return timeFrom;
    }

    /**
     * 获得指定日期的下一天
     *
     * @param day
     * @return
     */
    public static String getNextDayTime(String day) {
        Date date = DateUtils.stringToDate(day, DateConstant.DATE_TIME_SECOND);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);

        return new SimpleDateFormat(DateConstant.DATE_TIME_SECOND).format(cal.getTime());
    }

    /**
     * 获得指定日期的前一天
     *
     * @param day
     * @return
     */
    public static String getPreDayTime(String day) {
        Date date = DateUtils.stringToDate(day, DateConstant.DATE_TIME_SECOND);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);

        return new SimpleDateFormat(DateConstant.DATE_TIME_SECOND).format(cal.getTime());
    }

    /**
     * 多少天以后
     *
     * @param date
     * @param dayNum
     * @return
     */
    public static Date getNextPrevDay(Date date, int dayNum) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, dayNum);
        return calendar.getTime();
    }

    /**
     * 判断time1 是否早于time2
     *
     * @param time1
     * @param time2
     * @return
     */
    public static boolean before(String time1, String time2) {
        Date date1 = DateUtils.stringToDate(time1, DateConstant.DATE_TIME_SECOND);
        Date date2 = DateUtils.stringToDate(time2, DateConstant.DATE_TIME_SECOND);

        return date1.before(date2);
    }

    /**
     * 得到某个时间段加上多少小时的时间
     *
     * @param dateTime
     * @param hours
     * @return
     */
    public static Date getDateTimeAddHours(Date dateTime, Integer hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        calendar.add(Calendar.HOUR, hours);
        return calendar.getTime();
    }

    /**
     * 获取当前时间的时间戳
     *
     * @return
     */
    public static long getTimestampByDate() {
        return System.currentTimeMillis();
    }

    /**
     * 获取指定时间的时间戳
     *
     * @param dt
     * @return
     */
    public static long getTimestampByDate(Date dt) {
        return dt.getTime();
    }

    /**
     * 取得当前unix时间戳（精确到秒）
     *
     * @return unixTimeStamp
     */
    public static String getUnixTimeStamp() {
        long time = System.currentTimeMillis();
        String unixTimeStamp = String.valueOf(time / 1000);
        return unixTimeStamp;
    }

    /**
     * Java将Unix时间戳转换成指定格式日期字符串
     *
     * @param timestampString 时间戳 如："1473048265";
     * @param formats         要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static String unixTimeStamp2Date(String timestampString, String formats) {
        if (StringUtils.isBlank(formats)) {
            formats = DateConstant.DATE_TIME_SECOND;
        }
        Long timestamp = Long.parseLong(timestampString) * 1000;
        String date = new SimpleDateFormat(formats, Locale.CHINA).format(new Date(timestamp));
        return date;
    }


    /**
     * Java将Unix时间戳转换成指定格式日期
     *
     * @param timestampString 时间戳 如："1507701120000";
     * @param formats         要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static Date unixTimeStampDate(String timestampString, String formats) {
        if (StringUtils.isBlank(formats)) {
            formats = DateConstant.DATE_TIME_SECOND;
        }
        SimpleDateFormat format = new SimpleDateFormat(formats);
        Long time = new Long(timestampString);
        String d = format.format(time);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException p) {
			p.printStackTrace();
        }
        return date;
    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param dateStr 字符串日期
     * @param format  如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2UnixTimeStamp(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(dateStr).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 在时间上增加
     * 返回Date
     * @param ori
     * @param addIncr
     * @param unit
     * @return
     */
    public static Date add(Date ori, int addIncr, int unit) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTime(ori);
        cal.add(unit, addIncr);
        return cal.getTime();
    }

    /**
     * 在时间上增加
     * 返回String
     * @param ori
     * @param addIncr
     * @param unit
     * @param formatStyle
     * @return
     */
    public static String addWithString(Date ori, int addIncr, int unit, String formatStyle) {
        Date dt = add(ori, addIncr, unit);
        return new SimpleDateFormat(formatStyle).format(dt);
    }

    /**
     * 在时间上减少
     * 返回Date
     * @param ori
     * @param minusIncr
     * @param unit
     * @return
     */
    public static Date minus(Date ori, int minusIncr, int unit) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTime(ori);
        cal.add(unit, -minusIncr);
        return cal.getTime();
    }

    /**
     * 在时间上减少
     * 返回String
     * @param ori
     * @param minusIncr
     * @param unit
     * @param formatStyle
     * @return
     */
    public static String minusWithString(Date ori, int minusIncr, int unit, String formatStyle) {
        Date dt = minus(ori, minusIncr, unit);
        return new SimpleDateFormat(formatStyle).format(dt);
    }

}
