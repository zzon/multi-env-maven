/*
 * 创建时间：2017-09-03 21:37
 * 项目名称:kmall_pt
 * 类名称:StringUtils.java
 * 包名称:com.kmall.common.utils
 */
package com.songce.util;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 名称：StringUtils <br>
 * 描述：String工具类<br>
 *
 * @author Scott
 * @version 1.0
 * @since 1.0.0
 */
public class StringUtils {
    public static final String EMPTY = "";
    private static Pattern linePattern = Pattern.compile("_(\\w)");

    /**
     * 判断字符串是否不为空，不为空则返回true
     *
     * @param str 源数据
     * @return Boolean
     */
    public static boolean isNotEmpty(String str) {
        if (str != null && !"".equals(str.trim()) && !"null".equalsIgnoreCase(str)) {
            return true;
        }
        return false;
    }

    /**
     * 判断对象或对象数组中每一个对象是否为空: 对象为null，字符序列长度为0，集合类、Map为empty
     *
     * @param obj
     * @return
     */
    public static boolean isNullOrEmpty(Object obj) {
        if (obj == null) {
            return true;
        }

        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        }

        if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        }

        if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        }

        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (int i = 0; i < object.length; i++) {
                if (!isNullOrEmpty(object[i])) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }

    /**
     * 下划线转驼峰
     */
    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 校验数值只能是数字且可以带两位小数
     * @param value
     * @return
     */
    public static boolean checkNumberByTwoDecimal(String value){
        // 要验证的字符串
        String str = String.valueOf(value);
        // 带小数
        String regEx = "\\d+(\\.\\d{1,2})?";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        // 字符串是否与正则表达式相匹配
        boolean rs = matcher.matches();
        return rs;
    }

    /**
     * 校验数值只能是数字且只可以带一位小数
     * @param value
     * @return
     */
    public static boolean checkNumberByOneDecimal(String value){
        // 要验证的字符串
        String str = String.valueOf(value);
        // 带小数
        String regEx = "\\d+(\\.\\d{1,1})?";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        // 字符串是否与正则表达式相匹配
        boolean rs = matcher.matches();
        return rs;
    }

    /**
     * 校验数值只能是数字
     * @param value
     * @return
     */
    public static boolean checkNumberByInteger(String value){
        // 要验证的字符串
        String str = String.valueOf(value);
        // 带小数
        String regEx = "[+]{0,1}(\\d+)";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        // 字符串是否与正则表达式相匹配
        boolean rs = matcher.matches();
        return rs;
    }
    public static void main(String[] args) {

        boolean rs = checkNumberByTwoDecimal("1");
        System.out.println(rs);
        boolean rs2 = checkNumberByInteger("1");
        System.out.println(rs2);
        boolean rs3 = checkNumberByOneDecimal("1.11");
        System.out.println(rs3);
    }
}
