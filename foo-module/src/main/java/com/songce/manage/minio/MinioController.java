package com.songce.manage.minio;

import com.songce.common.PageUtils;
import com.songce.common.R;
import com.songce.manage.minio.entity.MinioProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 多环境打包测试
 */
@RestController
public class MinioController {
    private static final Logger logger = LoggerFactory.getLogger(MinioController.class);

    private MinioProperties minioProperties;

    @Autowired
    private void setMinioProperties(MinioProperties minioProperties) {
        this.minioProperties = minioProperties;
    }


    @RequestMapping(value = "/env/foo/host")
    public R envTest() {
        logger.debug("---------- 多环境打包测试 ----------");

        Map<String, String> map = new HashMap<>();
        List<Map> list = new ArrayList<>();

        String fooHost = minioProperties.getHost();
        String inited = minioProperties.getInited();
        String baseApi = minioProperties.getBaseApi();
        map.put("fooHost", fooHost);
        map.put("inited", inited);
        map.put("baseApi", baseApi);

        list.add(map);

        PageUtils pageUtil = new PageUtils(list, map.size(), 20, 1);
        return R.ok().put("page", pageUtil);
    }

}
