package com.songce.manage.minio.config.p1;

import com.songce.manage.minio.entity.MinioProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-25
 * @since 1.0.0
 */
@Profile({"p1-test"})
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/${env}/minio-${env}.properties"
}, encoding="UTF-8")
public class MinioTestConfiguration {

    @Value("${p1.test.minio.host}")
    private String host;
    @Value("${p1.test.minio.inited}")
    private String inited;
    @Value("${p1.test.minio.baseApi}")
    private String baseApi;

    @Bean
    public MinioProperties minioProperties() {
        MinioProperties properties = new MinioProperties();
        properties.setHost(host);
        properties.setInited(inited);
        properties.setBaseApi(baseApi);
        return properties;
    }

}
