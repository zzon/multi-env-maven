package com.songce.manage.minio.entity;

import java.io.Serializable;

public class MinioProperties implements Serializable {

    private String host;
    private String baseApi;
    private String inited;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBaseApi() {
        return baseApi;
    }

    public void setBaseApi(String baseApi) {
        this.baseApi = baseApi;
    }

    public String getInited() {
        return inited;
    }

    public void setInited(String inited) {
        this.inited = inited;
    }
}
