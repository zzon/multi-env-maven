package com.songce.manage.redis;

import java.io.Serializable;

/**
 * @author Scott Chen
 * @since 1.0
 * 2018-09-27
 */
public class JedisProperties implements Serializable {
    private static final long serialVersionUID = 8184312570045220343L;

    private String keyPrefix;
    private String host;
    private String port;
    private String password;
    private Pool pool;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }

    public static class Pool {
        private String maxIdle;
        private String maxWait;
        private String maxTotal;
        private String testOnBorrow;

        public String getMaxIdle() {
            return maxIdle;
        }

        public void setMaxIdle(String maxIdle) {
            this.maxIdle = maxIdle;
        }

        public String getMaxWait() {
            return maxWait;
        }

        public void setMaxWait(String maxWait) {
            this.maxWait = maxWait;
        }

        public String getMaxTotal() {
            return maxTotal;
        }

        public void setMaxTotal(String maxTotal) {
            this.maxTotal = maxTotal;
        }

        public String getTestOnBorrow() {
            return testOnBorrow;
        }

        public void setTestOnBorrow(String testOnBorrow) {
            this.testOnBorrow = testOnBorrow;
        }
    }

}
