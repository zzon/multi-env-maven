package com.songce.manage.redis.config.p2;

import com.songce.manage.redis.JedisProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Scott Chen
 * @date 2023-12-25
 * @since 1.0.0
 */
@Profile({"p2-prod"})
@Configuration
@PropertySource(value = {
        "classpath:conf/${platform}/${env}/redis-${env}.properties"
}, encoding="UTF-8")
public class RedisProdConfiguration {

    @Value("${p2.prod.redis.keyPrefix}")
    private String keyPrefix;
    @Value("${p2.prod.redis.host}")
    private String host;
    @Value("${p2.prod.redis.port}")
    private String port;
    @Value("${p2.prod.redis.password}")
    private String password;


    @Value("${p2.prod.redis.maxIdle}")
    private String maxIdle;
    @Value("${p2.prod.redis.maxWait}")
    private String maxWait;
    @Value("${p2.prod.redis.maxTotal}")
    private String maxTotal;
    @Value("${p2.prod.redis.testOnBorrow}")
    private String testOnBorrow;

    @Bean
    public JedisProperties redisProperties() {
        JedisProperties.Pool pool = new JedisProperties.Pool();
        pool.setMaxIdle(maxIdle);
        pool.setMaxWait(maxWait);
        pool.setMaxTotal(maxTotal);
        pool.setTestOnBorrow(testOnBorrow);

        JedisProperties properties = new JedisProperties();
        properties.setKeyPrefix(keyPrefix);
        properties.setHost(host);
        properties.setPort(port);
        properties.setPassword(password);
        properties.setPool(pool);

        return properties;
    }

}
