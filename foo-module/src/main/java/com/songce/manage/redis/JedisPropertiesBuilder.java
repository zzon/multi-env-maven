package com.songce.manage.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * @author Scott Chen
 * @since 1.0
 * 2018-09-27
 */
@Component
public class JedisPropertiesBuilder {
    private static final Logger logger = LoggerFactory.getLogger(JedisPropertiesBuilder.class);

    private JedisProperties jedisProperties;

    private static JedisProperties jedisProp;

    @Autowired
    public void setJedisProperties(JedisProperties jedisProperties) {
        this.jedisProperties = jedisProperties;
    }

    /**
     * Bean 向静态变量赋值
     */
    @PostConstruct
    private void init(){
        long start = System.currentTimeMillis();
        logger.info("JedisProperties初始化开始......");
        jedisProp = jedisProperties;
        logger.info("初始化Jedis连接池耗时: {} ms", System.currentTimeMillis() - start);
    }

    public static JedisProperties instance(){
        if (jedisProp == null) {
            logger.error("JedisProperties实例为空");
            throw new RuntimeException("JedisProperties实例为空");
        } else {
            logger.error("JedisProperties实例不为空");
        }
        logger.info("JedisProperties实例返回");
        return jedisProp;
    }

}
