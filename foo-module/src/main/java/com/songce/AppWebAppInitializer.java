package com.songce;

import com.songce.config.spring.RootConfig;
import com.songce.config.spring.ServletConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * 编程方式实现 Web.xml中 ContextLoaderListener 与 DispatcherServlet 配置
 *
 * @author Scott Chen
 * @date 2023-12-27
 * @since 1.0.0
 */
public class AppWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 创建 root application context
     *
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{
            RootConfig.class};
    }

    /**
     * 创建 Servlet application context
     *
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{
                ServletConfig.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

}
